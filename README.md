# B3 - Desafio Técnico #

### Atividades

* Arquitetura :white_check_mark:
* Detalhamento e explicações :white_check_mark:
* Desenvolvimento :x:

### Arquitetura

**OBS: Para melhor visualização, utilize o arquivo _b3-desafio-tecnico-arquitetura.html_. Para isso, clone o projeto e abra o arquivo HTML no seu browser


![](b3-desafio-tecnico-arquitetura.jpg)


### Contextualização

Considerando os requisitos, optei por utilizar:

* Arquitetura orientada a microservices
* gRPC como protocolo de integração entre aplicações
* Ambiente de execução Service Mesh com Istio
* Redis para armazenamento dos dados com persistência em disco
* [Dynomite](https://github.com/Netflix/dynomite) para replicação dos dados do Redis, garantindo alta disponibilidade em alta performance
* Log Analytics utilizando Elastic Stack (Elastic Search, Kibana, Logstach etc)
* Grafana e Prometheus para telemetria
* Kafka como Broker de mensagem
* MongoDB para banco de dados para fins de leitura e métricas de eficiência operacional

### Detalhamento
* Todas as aplicações serão executadas em ambiente de execução Kubernetes com Service Mesh, utilizando Istio. Isso já dará nativamente um ótimo nível de observabilidade (métricas, logs e eventos), para fins de uptime, disponibilidade de cada instância de aplicação, geração de indicadores, alertas e troubleshooting. As ferramentas utilizadas aqui são: Grafana, Prometheus e Elastic Stack. 
* As aplicações terão estratégia de auto scalling nativamente com Kubernetes.
* Auth server será responsável por autenticação e autorização - usando REST, utilizado pelas aplicações expostas externamente.
* As aplicações de _cadastro_ e _precificação_, utilizarão o Redis como storage. No startup da aplicação, será lidos os dados no Redis e preenchido um cache em memória para evitar chamadas remotas durante a execução do processo. Para garantir que o cache em memória sempre estará atualizado, utilizaremos a feature do Redis chamada [Redis Notification](https://redis.io/topics/notifications), a qual permite termos Listener em cada instância de aplicação que receberá notificação de alteração de dados. 
* Como base de dados para consulta via API e geração de alertas de volume financeiro, utilizaremos um Cluster MongoDB, que estará externo do Cluster Kubernetes.

### Aplicações e responsabilidades
Abaixo as aplicações existentes e suas responsabilidades.

- _Scheduler Cadastral_: Responsável por ler arquivo cadastral delimitado e persistir no Cluster Redis.
- _Cadastro_: Responsável por realizar as validações cadastrais em tempo de processamento de um nova ordem de compra. Essa aplicação utilizará o Cluster Redis como fonte de dados, a ideia é _1) em momento de startup_ a aplicação leia os dados do Redis e gere um cache na sua própria JVM, com isso não será realizada chamada externa em momento de validação e caso a instância reiniciei ou haja auto scalling, a instância poderá preencher o cache em JVM, _2) caso haja alteração de dados cadastrais durante o dia_ e para garantir que o cache em memória sempre estará atualizado, utilizaremos a feature do Redis chamada [Redis Notification](https://redis.io/topics/notifications).
- _Precificação_: Responsável pela precificação de ativo. As instâncias dessa aplicação também utilizarão a estratégia de [Redis Notification](https://redis.io/topics/notifications), afim de evitar chamadas externas para cada nova requisição. 
- _Pedidos_: Responsável pela _1) validação de pedidos duplicados_, utilizando um storage externo, garantindo que todas as instâncias da aplicação utilizarão a mesma estrutura de dados para validação. E, _2) publicação de pedidos no Kafka_, para fins de persistir dados na base utilizada pela API e geração de alertas.
- _Auth Server_: Responsável por autenticação e autorização, tendo como aplicações clients: Processador e API Gateway.
- _Processador_: Responsável pela orquestração dos processos core, sendo ela a aplicação que receberá toda e qualquer requisição transacional das aplicações externas. Tais processos estarão encapsulados a um processo de retry, que garantirá uma determinada quantia de retentativa (através de variável de ambiente). Essa aplicação integrará internamente com:
    - Auth Server
    - Cadastro
    - Precificação
    - Pedidos
- _Consolidar pedidos_: Responsável por consumir as mensagem do Kafka (as quais se referem aos pedidos realizados), e persistir num Cluster de MongoDB, o qual será responsável por disponibilizar os dados real time para consulta via API. Essa aplicação terá N instâncias em execução, garantindo e suportante a estratégia de paralelismo dos tópicos do Kafka.
- _Consolidador de volume financeiro_: Responsável por notificaor os clientes que tiverem uma volume financeiro maior que R$ 50 milhões em um intervalo de tempo (30 minutos). Essa aplicação utilizará o recurso de [aggregate](https://docs.mongodb.com/manual/reference/method/db.collection.aggregate/) do MongoDB.
- _API - Pedidos_: Responsável por disponibilizar endpoints de consulta real time aos clientes externos. Para exposicão externamente teremos:
    - WAF
    - Load Balancer
    - API Gateway
    - Auth Server
